function Input({ placeholder, onChange, id, labelText= '', type = 'text'}) {
    return (
        <div>
            <label htmlFor={id}>{labelText}</label>
            <input type={type} placeholder={placeholder} onChange={onChange} id={id}
            />
        </div>
    );
}
export default Input;