import Header from "./components/Header/Header";
import Courses from "./components/Courses/Courses";
import './App.css'

function App () {
    return (
        <div className='app-container'>
        <Header />
        <Courses />
    </div>
    );
}
 export default App;