import React, { useState, useEffect} from "react";
import { v4 as uuuidv4 } from 'uuid';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import { mockedAuthorsList } from '../../data/authors';
import './CreateCourse.css'

function CreateCourse ({ onAddCourse, onClose, allAuthors, onAddAuthor}) {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [duration, setDuration] = useState('');
    const [authors, setAuthors] = useState(mockedAuthorsList);
    const [courseAuthors, setCourseAuthors] = useState([]);
    const [newAuthorName, setNewAuthorName] = useState('');
    const [availableAuthors, setAvailableAuthors] = useState(allAuthors);
    const [formattedDuration, setFormattedDuration] = useState('');

    useEffect(() => {
        setAuthors(allAuthors);
    },[allAuthors]);

    useEffect(() => {
        const formatDuration = (duration) => {
            const hours = Math.floor(duration / 60);
            const minutes = duration % 60;
            return `Duration: ${hours}:${minutes.toString().padStart(2, '0')} hours`;
        };

        setFormattedDuration(formatDuration(duration));
    }, [duration]);

    const handleAddAuthorToCourse = (author) => {
        setAuthors(authors.filter((item) => item.id !== author.id));
        setCourseAuthors([...courseAuthors, author]);
    };

    const handleRemoveAuthorFromCourse = (author) => {
        setCourseAuthors(courseAuthors.filter((item) => item.id !== author.id));
        setAuthors([...authors, author]);
    };

    const handLeCreateAuthor =  () => {
        if (newAuthorName.length < 2) {
            alert('Author name must be at least 2 characters long');
            return;
        }
        const newAuthor = { id: uuuidv4(), name: newAuthorName};
        onAddAuthor(newAuthor);
        setNewAuthorName('');
    };

    const handleAddAuthor = (authorId) => {
        const authorToAdd = authors.find((author) => author.id === authorId);
        if (!authorToAdd) return;

        setAuthors(authors.filter((author) => author.id !== authorId));
        setCourseAuthors([...courseAuthors, authorToAdd]);
        setAvailableAuthors(
            availableAuthors.filter((author) => author.id !== authorId)
        );
    };

    const handleDeleteAuthor = (authorId) => {
        const authorToDelete = courseAuthors.find(
            (author) => author.id === authorId
        );

        if (!authorToDelete) return;

        setCourseAuthors(courseAuthors.filter((author) => author.id !== authorId));
        setAuthors([...authors, authorToDelete]);
        const author = allAuthors.find((author) => author.id === authorId);
        setAvailableAuthors([...availableAuthors, author]);
};

    const renderAuthorList = () => {
            return authors.map((author) =>(<div key={author.id} className='author'>
                <span>{author.name}</span>
                <Button
                    buttonText='Add Author'
                    onClick={() => handleAddAuthor(author.id)}
            />
            </div>
            ));
    };

    const renderCourseAuthorList = () => {
        return courseAuthors.map((author) => (
            <div key={author.id} className='author'>
                <span>{author.name}></span>
                <Button
                    buttonText='Delete Author'
                    onClick={() => handleDeleteAuthor(author.id)}
                    />
            </div>
        ));
    };

    const handleSubmit = () => {
        if (
            title === '' ||
            description === '' ||
            duration === '' ||
            courseAuthors.length === 0
        ) {
            alert ('All fields are required');
            return;
        }

        const newCourse = {
            id: uuuidv4 (),
            title,
            description,
            creationDate: new Date ().toLocaleDateString('en-GB'),
            duration: parseInt(duration),
            authors: courseAuthors.map((author) => author.id),
        };

        const renderCourseAuthorsList = () => {
            return courseAuthors.map ((author) => (
                <div key={author.id} className='author'>
                    <span>{author.name}</span>
                    <Button
                        buttonText='Detele Author'
                        onClick ={() => handleRemoveAuthorFromCourse(author)}
                    />
                </div>
            ));
        };
        onAddCourse(newCourse);
        onClose();
    };

    return (
        <div className='create-course'>
            <div className='course-top-row'>
                <Input

                        placeholder='Enter title'
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                        id='courseTitle'
                        labelText={'Title'}
        />
        <Button buttonText='Create Course' onClick={handleSubmit} />
        <Button buttonText='cancel' onClick={onClose} />
        </div>
            <label htmlFor={description}>Description</label>
            <textarea
                        className='course-description'
                        placeholder='Description'
                        value={description}
                        id='description'
                        onChange={(e) => setDescription(e.target.value)}
                        />
            <div className='course-authors-duration'>
                <div className='left-side'>
                    <div className='bottom-gap'>
                    <h3> Add Author</h3>
                    <Input
                        placeholder='Author Name'
                        value={newAuthorName}
                        onChange={(e) => setNewAuthorName(e.target.value)}
                        id='authorName'
                        lebelText='Author Name'
                        />
                </div>
                    <div className='bottom-gap'>
                        <Button buttonText='Cretae Author' onClick={handLeCreateAuthor} />
                </div>

                <div className='bottom-gap '>
                    <Input
                        type='number'
                        placeholder='Duration (minutes)'
                        value= {duration}
                        onChange={(e) => setDuration(e.target.value)}
                        id='duration'
                        labelText='Duration'
                        />

                </div>
                <div className='formatted-duration'>{formattedDuration}</div>

            </div>

            <div className='right.side '>
                <h3>Authors</h3>
                <div className='authors-list'>{renderAuthorList()}</div>
                <h3>Course Authors</h3>
                <div className='course-authors-list'>{renderCourseAuthorList()}
                </div>
            </div>
        </div>
    </div>
    );
}
export default CreateCourse;


