import React from "react";
import Logo from "./components/Logo/Logo";
import './Header.css';
import Button from '../../common/Button/Button';

function Header () {
    return (
        <header className='header-container'>
            <Logo />
            <div className='header-text-button-container'>
                <span className='header-text'> Paola Caballero</span>
                <Button buttonText='Logout' />
            </div>
        </header>
    );
}
export default Header;