import React from 'react';
import Button from "../../../../common/Button/Button";
import Input from '../../../../common/Input/Input';
import './SearchBar.css';

function SearchBar({ onSearch }) {
    const [searchQuery, setSearchQuery] = React.useState('');

    const handleSearch = () => {
        onSearch(searchQuery);
    };

    return (
        <div className='search-bar'>
            <Input
                placeholder='Enter course name or id'
                onChange={(e) => setSearchQuery(e.target.value)}
            />
            <Button buttonText='Search' onClick={handleSearch} />
        </div>
    );
}

export default SearchBar;
