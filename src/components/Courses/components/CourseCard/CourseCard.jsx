import './CourseCard.css';

function CourseCard ({ course, authors}) {
    const formatDuration = (duration) => {
        const hours = Math.floor(duration / 60);
        const minutes = duration % 60;
        return `${hours}:${minutes.toString().padStart(2, '0')} hours`;
    };

    const getAuthors = (authorsIds) => {
        return authorsIds
            .map((id) => authors.find((author) => author.id === id)?.name)
            .join(',');
    };

    return (
        <div className='course-card'>
            <div className='course-info-left'>
            <h3 className='course-title'>{course.title}</h3>
            <p className='course-description'>
                {course.description}</p>
        </div>

            <div className='course-info-rigth'>
                <p className={'course-authors'}> Authors:
                {getAuthors(course.authors)}</p>

                <p className='course-duration'>
                    Duration: {formatDuration(course.duration)}
                </p>
                <p className='course-date'>Created:
                    {course.creationDate}</p>
                <button> Show course </button>
            </div>
        </div>
    );
}

export default CourseCard;