import searchBar from "./components/SearchBar/SearchBar";
import React, { useState, useEffect } from "react";
import CourseCard from "./components/CourseCard/CourseCard";
import Button from "../../common/Button/Button";
import {mockedCoursesList} from '../../data/courses';
import { mockedAuthorsList} from '../../data/authors'
import CreateCourse from '../CreateCourse/CreateCourse';
import './Courses.css';
import SearchBar from "./components/SearchBar/SearchBar";

function Courses () {
    const [searchQuery, setSearchQuery] = useState('');
    const [courses, setCourses] = useState('mockedCoursesList');
    const [filteredCourses, setFilteredCourses] = useState(mockedCoursesList);
    const [showCreateCourse, setShowCreateCourses] = useState(false);
    const [authors, setAuthors] = useState(mockedAuthorsList);
    const handLeAddCourse = (newCourse) => {
        setCourses([newCourse, ...courses]);
        setFilteredCourses([newCourse, ...filteredCourses]);
    };

    useEffect(() => {
        setAuthors(mockedAuthorsList);
    },[]);

    const handleAddAuthor = (newAuthor) => {
        setAuthors((prevAuthors) => [...prevAuthors, newAuthor]);
    };

    const handLeSearch = (query) => {
        if (query) {
            setFilteredCourses(
                mockedCoursesList.filter(
                    (course) =>
                        course.title.toLowerCase().includes(query.toLowerCase()) || course.id.toLowerCase().includes(query.toLowerCase())));
        }
        else {
            setFilteredCourses(mockedCoursesList);
        }
    };

    return (
        <div className='courses'>
            { showCreateCourse ? (
                <div className='courses-list'>
                    <CreateCourse
                        onAddCourse={handLeAddCourse}
                        onClose={() => setShowCreateCourses(false)}
                        allAuthors={authors}
                        onAddAuthor={handleAddAuthor}
                    />
                </div>
            ) : (
            <>
                <div className='courses-top'>
                    <SearchBar onSearch={handLeSearch} />
                    <Button
                        buttonText='Add New Course'
                        onClick={() => setShowCreateCourses(true)}
                        />
                </div>
                <div className='courses-list'>
                    {filteredCourses.map((course) => (
                        <CourseCard key={course.id} course={course} authors={authors}  />
                    ))}
                </div>
            </>
        )}
        </div>
    );
}

export default Courses;


